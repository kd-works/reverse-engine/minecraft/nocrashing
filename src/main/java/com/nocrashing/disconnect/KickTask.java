package com.nocrashing.disconnect;

import com.nocrashing.disconnect.DisconnectManager;

import java.util.Iterator;

public class KickTask extends Thread {
    private final DisconnectManager dm;

    public KickTask(DisconnectManager disconnectManager) {
        this.dm = disconnectManager;

        this.start();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void run() {
        do {
            try {
                KickTask.EQ("-k3962o", (long)1000L);
            } catch (InterruptedException interruptedException) {
                KickTask.EQ("-1cm5627", (Object)interruptedException);
            }

            if (KickTask.EQ("-110362p", (Object)this.dm).isEmpty()) {
                return;
            }

            Object object = KickTask.EQ("-110362p", (Object)this.dm);

            synchronized (object) {
                Iterator iterator = KickTask.EQ("-110362p", (Object)this.dm).iterator();

                while (iterator.hasNext()) {
                    String string = (String)iterator.next();
                    Object object2 = KickTask.EQ("-193n63e", (Object)string);

                    if (object2 != null) {
                        KickTask.EQ("-1j2b62s", (Object)this.dm, (Object)object2);
                        continue;
                    }

                    iterator.remove();
                }
            }
        } while (true);
    }
}
