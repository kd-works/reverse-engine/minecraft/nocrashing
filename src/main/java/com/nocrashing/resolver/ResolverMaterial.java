package com.nocrashing.resolver;

import com.nocrashing.resolver.MaterialResolver;

import net.minecraft.server.v1_8_R3.ItemStack;

public class ResolverMaterial {
    private final ItemStack itemStack;

    public ResolverMaterial(ItemStack itemStack) {
        this.itemStack = itemStack;
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    public MaterialResolver getMaterialResolver() {
        if (this.itemStack == null) {
            return null;
        }

        Object object = ResolverMaterial.Qp("-b5dno2", (Object)ResolverMaterial.Qp("-1ichnnv", (Object)ResolverMaterial.Qp("-bs3no0", (Object)this.itemStack)), (Object)"item.", (Object)"");

        switch (ResolverMaterial.Qp("-3p3no1", (Object)object)) {
            case -1946719072: {
                if (ResolverMaterial.Qp("-e9bno4", (Object)object, (Object)"writtenBook") != false) return MaterialResolver.WRITTEN_BOOK;
                return null;
            }
            case 3029737: {
                if (ResolverMaterial.Qp("-e9bno4", (Object)object, (Object)"book") != false) return MaterialResolver.BOOK;
                return null;
            }
            case 1426840037: {
                if (ResolverMaterial.Qp("-e9bno4", (Object)object, (Object)"writingBook") != false) return MaterialResolver.WRITING_BOOK;
                return null;
            }
        }

        return null;
    }
}
