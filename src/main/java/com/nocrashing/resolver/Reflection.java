package com.nocrashing.resolver;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.regex.Pattern;

public final class Reflection {
    private static String OBC_PREFIX = Reflection.Lf("1po888f", (Object)Reflection.Lf("-1m1dnni", (Object)Reflection.Lf("-13a5nnn", (Object)Reflection.Lf("-8u7nnf"))));
    private static String NMS_PREFIX = Reflection.Lf("-b5dno2", (Object)OBC_PREFIX, (Object)"org.bukkit.craftbukkit", (Object)"net.minecraft.server");
    private static String VERSION = Reflection.Lf("-b5dno2", (Object)Reflection.Lf("-b5dno2", (Object)OBC_PREFIX, (Object)"org.bukkit.craftbukkit", (Object)""), (Object)".", (Object)"");
    private static Pattern MATCH_VARIABLE = Reflection.Lf("-1icnnp5", (Object)"\\{([^\\}]+)\\}");

    private Reflection() {
    }

    public static <T> FieldAccessor<T> getField(Class<?> class_, String string, Class<T> class_2) {
        return Reflection.getField(class_, string, class_2, 0);
    }

    public static <T> FieldAccessor<T> getField(String string, String string2, Class<T> class_) {
        return Reflection.getField(Reflection.getClass(string), string2, class_, 0);
    }

    public static <T> FieldAccessor<T> getField(Class<?> class_, Class<T> class_2, int n) {
        return Reflection.getField(class_, null, class_2, n);
    }

    public static <T> FieldAccessor<T> getField(String string, Class<T> class_, int n) {
        return Reflection.getField(Reflection.getClass(string), class_, n);
    }

    private static <T> FieldAccessor<T> getField(Class<?> class_, String string, Class<T> class_2, int n) {
        for (final Field field : (Field[])Reflection.Lf("1r486p", class_)) {
            if (string != null && Reflection.Lf("-e9bno4", (Object)Reflection.Lf("18ua86m", (Object)field), (Object)string) == false || Reflection.Lf("-1khlnnq", class_2, (Object)Reflection.Lf("ck486n", (Object)field)) == false || n-- > 0) continue;

            Reflection.Lf("76486k", (Object)field, (boolean)true);

            return new FieldAccessor<T>(){
                @Override
                public T get(Object object) {
                    try {
                        return _1.KE("-9r7nnm", (Object)field, (Object)object);
                    } catch (IllegalAccessException illegalAccessException) {
                        throw new RuntimeException("Cannot access reflection.", illegalAccessException);
                    }
                }

                @Override
                public void set(Object object, Object object2) {
                    try {
                        _1.KE("-93tnnl", (Object)field, (Object)object, (Object)object2);
                    } catch (IllegalAccessException illegalAccessException) {
                        throw new RuntimeException("Cannot access reflection.", illegalAccessException);
                    }
                }

                @Override
                public boolean hasField(Object object) {
                    return (boolean)_1.KE("-1khlnnq", (Object)_1.KE("-561nno", (Object)field), (Object)_1.KE("-13a5nnn", (Object)object));
                }
            };
        }

        if (Reflection.Lf("1s5u86l", class_) != null) {
            return Reflection.getField(Reflection.Lf("1s5u86l", class_), string, class_2, n);
        }

        throw new IllegalArgumentException((String)Reflection.Lf("-nvvnne", (Object)Reflection.Lf("dgg885", (Object)new StringBuilder("Cannot find field with type "), class_2)));
    }

    public static MethodInvoker getMethod(String string, String string2, Class<?> ... arrclass) {
        return Reflection.getTypedMethod(Reflection.getClass(string), string2, null, arrclass);
    }

    public static MethodInvoker getMethod(Class<?> class_, String string, Class<?> ... arrclass) {
        return Reflection.getTypedMethod(class_, string, null, arrclass);
    }

    public static MethodInvoker getTypedMethod(Class<?> class_, String string, Class<?> class_2, Class<?> ... arrclass) {
        for (final Method method : (Method[])Reflection.Lf("q74892", class_)) {
            if ((string != null && Reflection.Lf("-e9bno4", (Object)Reflection.Lf("-1er7nmt", (Object)method), (Object)string) == false || class_2 != null) && (Reflection.Lf("87e891", (Object)Reflection.Lf("1d7g890", (Object)method), class_2) == false || Reflection.Lf("-108pnn1", (Object[])((Class[])Reflection.Lf("-1vapnn2", (Object)method)), (Object[])arrclass) == false)) continue;

            Reflection.Lf("29888s", (Object)method, (boolean)true);

            return new MethodInvoker(){
                @Override
                public Object invoke(Object object, Object ... arrobject) {
                    try {
                        return _2.Be("-fibnns", (Object)method, (Object)object, (Object[])arrobject);
                    } catch (Exception exception) {
                        throw new RuntimeException((String)_2.Be("-nvvnne", (Object)_2.Be("dgg885", (Object)new StringBuilder("Cannot invoke method "), (Object)method)), exception);
                    }
                }
            };
        }

        if (Reflection.Lf("1s5u86l", class_) != null) {
            return Reflection.getMethod(Reflection.Lf("1s5u86l", class_), string, arrclass);
        }

        throw new IllegalStateException((String)Reflection.Lf("-11a9nmm", (Object)"Unable to find method %s (%s).", (Object[])new Object[]{string, Reflection.Lf("-1cp5nn3", (Object[])arrclass)}));
    }

    public static ConstructorInvoker getConstructor(String string, Class<?> ... arrclass) {
        return Reflection.getConstructor(Reflection.getClass(string), arrclass);
    }

    public static ConstructorInvoker getConstructor(Class<?> class_, Class<?> ... arrclass) {
        for (final Constructor constructor : (Constructor[])Reflection.Lf("-lc9nml", class_)) {
            if (Reflection.Lf("-108pnn1", (Object[])((Class[])Reflection.Lf("-1uh5nmo", (Object)constructor)), (Object[])arrclass) == false) continue;

            Reflection.Lf("otm899", (Object)constructor, (boolean)true);

            return new ConstructorInvoker(){
                @Override
                public Object invoke(Object ... arrobject) {
                    try {
                        return _3.zv("-m7lnng", (Object)constructor, (Object[])arrobject);
                    } catch (Exception exception) {
                        throw new RuntimeException((String)_3.zv("-nvvnne", (Object)_3.zv("dgg885", (Object)new StringBuilder("Cannot invoke constructor "), (Object)constructor)), exception);
                    }
                }
            };
        }

        throw new IllegalStateException((String)Reflection.Lf("-11a9nmm", (Object)"Unable to find constructor for %s (%s).", (Object[])new Object[]{class_, Reflection.Lf("-1cp5nn3", (Object[])arrclass)}));
    }

    public static Class<Object> getUntypedClass(String string) {
        Class<Object> class_ = Reflection.getClass(string);
        return class_;
    }

    public static Class<?> getClass(String string) {
        return Reflection.getCanonicalClass(Reflection.expandVariables(string));
    }

    public static Class<?> getNewClass(String string) {
        return Reflection.getCanonicalClass(string);
    }

    public static Class<?> getMinecraftClass(String string) {
        return Reflection.getCanonicalClass((String)Reflection.Lf("-nvvnne", (Object)Reflection.Lf("-htpnmp", (Object)Reflection.Lf("-htpnmp", (Object)new StringBuilder((String)Reflection.Lf("1d32896", (Object)NMS_PREFIX)), (Object)"."), (Object)string)));
    }

    public static Class<?> getCraftBukkitClass(String string) {
        return Reflection.getCanonicalClass((String)Reflection.Lf("-nvvnne", (Object)Reflection.Lf("-htpnmp", (Object)Reflection.Lf("-htpnmp", (Object)new StringBuilder((String)Reflection.Lf("1d32896", (Object)OBC_PREFIX)), (Object)"."), (Object)string)));
    }

    private static Class<?> getCanonicalClass(String string) {
        try {
            return Reflection.Lf("-173lnms", (Object)string);
        } catch (ClassNotFoundException classNotFoundException) {
            throw new IllegalArgumentException((String)Reflection.Lf("-nvvnne", (Object)Reflection.Lf("-htpnmp", (Object)new StringBuilder("Cannot find "), (Object)string)), classNotFoundException);
        }
    }

    private static String expandVariables(String string) {
        StringBuffer stringBuffer = new StringBuffer();
        Object object = Reflection.Lf("1ghg895", (Object)MATCH_VARIABLE, (Object)string);

        while (Reflection.Lf("-10t5ckk", (Object)object) != false) {
            Object object2 = Reflection.Lf("-1ftrnme", (Object)object, (int)1);
            Object object3 = "";

            if (Reflection.Lf("-h4hnmd", (Object)"nms", (Object)object2) != false) {
                object3 = NMS_PREFIX;
            } else if (Reflection.Lf("-h4hnmd", (Object)"obc", (Object)object2) != false) {
                object3 = OBC_PREFIX;
            } else if (Reflection.Lf("-h4hnmd", (Object)"version", (Object)object2) != false) {
                object3 = VERSION;
            } else {
                throw new IllegalArgumentException((String)Reflection.Lf("-nvvnne", (Object)Reflection.Lf("-htpnmp", (Object)new StringBuilder("Unknown variable: "), (Object)object2)));
            }

            if (Reflection.Lf("-15vtnm8", (Object)object3) > 0 && Reflection.Lf("-1f0hnmg", (Object)object) < Reflection.Lf("-15vtnm8", (Object)string) && Reflection.Lf("-lornmf", (Object)string, (int)Reflection.Lf("-1f0hnmg", (Object)object)) != 46) {
                object3 = Reflection.Lf("-nvvnne", (Object)Reflection.Lf("-htpnmp", (Object)new StringBuilder((String)Reflection.Lf("1d32896", (Object)object3)), (Object)"."));
            }

            Reflection.Lf("-1mj1nmh", (Object)object, (Object)stringBuffer, (Object)Reflection.Lf("-scjnmi", (Object)object3));
        }

        Reflection.Lf("-j4bckj", (Object)object, (Object)stringBuffer);
        return Reflection.Lf("-1hkpck6", (Object)stringBuffer);
    }

    public static interface ConstructorInvoker {
        public Object invoke(Object ... var1);
    }

    public static interface FieldAccessor<T> {
        public T get(Object var1);

        public void set(Object var1, Object var2);

        public boolean hasField(Object var1);
    }

    public static interface MethodInvoker {
        public Object invoke(Object var1, Object ... var2);
    }
}
