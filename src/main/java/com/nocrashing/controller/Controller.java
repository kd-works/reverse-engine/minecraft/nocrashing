package com.nocrashing.controller;

import com.nocrashing.NoCrashing;
import com.nocrashing.packets.Packet;
import com.nocrashing.patch.BlockExploit;
import com.nocrashing.patch.BookExploit;
import com.nocrashing.patch.CustomPayloadExploit;
import com.nocrashing.patch.SignExploit;
import com.nocrashing.patch.TabExploit;
import com.nocrashing.patch.WindowClickExploit;

public class Controller {
    private final NoCrashing main;

    public Controller(NoCrashing noCrashing) {
        this.main = noCrashing;

        this.setClass();
    }

    private void setClass() {
        new Packet(this.main);
        new BlockExploit(this.main);
        new BookExploit(this.main);
        new SignExploit(this.main);
        new TabExploit(this.main);
        new WindowClickExploit(this.main);
        new CustomPayloadExploit(this.main);
    }
}
