package com.nocrashing.packets;

import com.nocrashing.packets.ReadPacket;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;

import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandlerContext;

public class PacketListener extends ChannelDuplexHandler {
    private Player p;

    public PacketListener(Player player) {
        this.p = player;
    }

    public void channelRead(ChannelHandlerContext channelHandlerContext, Object object) throws Exception {
        ReadPacket readPacket = new ReadPacket(this.p, object);
        PacketListener.HS("-1gpeoo8").callEvent((Event)readPacket);

        if (PacketListener.HS("v2t77r", (Object)readPacket) != false) {
            return;
        }

        super.channelRead(channelHandlerContext, object);
    }
}
